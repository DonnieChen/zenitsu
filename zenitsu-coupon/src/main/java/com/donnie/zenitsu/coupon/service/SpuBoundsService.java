package com.donnie.zenitsu.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donnie.common.utils.PageUtils;
import com.donnie.zenitsu.coupon.entity.SpuBoundsEntity;

import java.util.Map;

/**
 * 商品spu积分设置
 *
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 00:34:30
 */
public interface SpuBoundsService extends IService<SpuBoundsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

