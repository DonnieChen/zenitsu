package com.donnie.zenitsu.coupon.dao;

import com.donnie.zenitsu.coupon.entity.CouponSpuRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券与产品关联
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 00:34:30
 */
@Mapper
public interface CouponSpuRelationDao extends BaseMapper<CouponSpuRelationEntity> {
	
}
