package com.donnie.zenitsu.coupon;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/*
*  如何使用Nacos作为配置中心统一管理配置
*    1. 引入依赖
*      <dependency>
          <groupId>com.alibaba.cloud</groupId>
          <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
       </dependency>
*    2. 创建bootstrap.properties
*		spring.application.name=zenitsu-coupon-microService
   	    spring.cloud.nacos.config.server-addr=192.168.0.109:8848
*    3. 在Nacos配置中心添加一个 数据集(Data Id)：zenitsu-coupon-microService.properties
* 		默认规则：应用名.properties
*    4. 给 应用名.properties 添加配置
*    5. 动态获取配置
* 		@RefreshScope: 动态获取并刷新配置
*       @Value("${配置项的名字}")：在代码中获取到配置
*    6. 如果Nacos配置中心和当前应用的配置文件中都配置了相同的配置项，以Nacos配置中心的配置优先
*
* 细节
*   1. 命名空间，配置隔离
* 		默认：public(保留空间)：默认新增的所有配置都在public空间
* 			1。开发，测试，生产：利用不同场景的命名空间名字，来区分和隔离环境
* 				注意：在 bootstrap.properties 中，需要界定使用哪个命名空间下的配置
* 				spring.cloud.nacos.config.namespace=cccd1b91-982d-4ce0-983d-1588a82837eb
* 			2. 每一个微服务之间相互隔离，每一个微服务都创建自己的命名空间，只加载自己命名空间下的所有配置
*   2. 配置集：所有的配置的集合
*   3. 配置集ID：与 properties 文件名齐名
*   4. 配置分组：
* 		默认所有配置集都属于：DEFAULT_GROUP
*
* 每一个微服务创建自己的命名空间，使用配置分组区分环境：test，dev，prod
*
* 同时加载多个配置集
* 	1. 微服务任何配置信息，任何配置文件都可以放在配置中心
*   2. 只需要在bootstrap.properties文件中说明加载配置中心哪些配置文件即可
*   3. @Value，@ConfigurationProperties
* 		对于SpringBoot在过去如何能通过以上注解拿到配置的值，现在也能通过配置中心拿到
*		并且，优先读取配置中心的配置值
* */
@EnableDiscoveryClient
@MapperScan("com.donnie.zenitsu.coupon.dao")
@SpringBootApplication
public class CouponApplication {

	public static void main(String[] args) {
		SpringApplication.run(CouponApplication.class, args);
	}

}
