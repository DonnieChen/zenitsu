package com.donnie.zenitsu.coupon.dao;

import com.donnie.zenitsu.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 00:34:31
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
