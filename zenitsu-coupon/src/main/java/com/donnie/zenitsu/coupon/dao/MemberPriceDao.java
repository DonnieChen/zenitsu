package com.donnie.zenitsu.coupon.dao;

import com.donnie.zenitsu.coupon.entity.MemberPriceEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品会员价格
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 00:34:30
 */
@Mapper
public interface MemberPriceDao extends BaseMapper<MemberPriceEntity> {
	
}
