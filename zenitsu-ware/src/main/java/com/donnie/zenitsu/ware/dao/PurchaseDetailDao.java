package com.donnie.zenitsu.ware.dao;

import com.donnie.zenitsu.ware.entity.PurchaseDetailEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-17 21:55:16
 */
@Mapper
public interface PurchaseDetailDao extends BaseMapper<PurchaseDetailEntity> {
	
}
