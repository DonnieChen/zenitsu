package com.donnie.zenitsu.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donnie.common.utils.PageUtils;
import com.donnie.zenitsu.ware.entity.PurchaseDetailEntity;

import java.util.Map;

/**
 * 
 *
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-17 21:55:16
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

