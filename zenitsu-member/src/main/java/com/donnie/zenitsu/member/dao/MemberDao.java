package com.donnie.zenitsu.member.dao;

import com.donnie.zenitsu.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-17 21:33:37
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
