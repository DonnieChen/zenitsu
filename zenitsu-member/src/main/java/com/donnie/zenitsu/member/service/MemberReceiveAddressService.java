package com.donnie.zenitsu.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donnie.common.utils.PageUtils;
import com.donnie.zenitsu.member.entity.MemberReceiveAddressEntity;

import java.util.Map;

/**
 * 会员收货地址
 *
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-17 21:33:36
 */
public interface MemberReceiveAddressService extends IService<MemberReceiveAddressEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

