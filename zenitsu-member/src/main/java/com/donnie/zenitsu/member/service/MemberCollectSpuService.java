package com.donnie.zenitsu.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donnie.common.utils.PageUtils;
import com.donnie.zenitsu.member.entity.MemberCollectSpuEntity;

import java.util.Map;

/**
 * 会员收藏的商品
 *
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-17 21:33:37
 */
public interface MemberCollectSpuService extends IService<MemberCollectSpuEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

