package com.donnie.zenitsu.member;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/*
 *  1.调用远程服务的步骤
 *    1.1引入open-feign
 *    2.1编写一个接口，告诉SpringCloud这个接口需要调用远程服务
 *    3.1声明接口的每一个方法都是调用远程服务的哪一个方法
 *  2.开启远程调用功能
 * */
@EnableFeignClients(basePackages = "com.donnie.zenitsu.member.feign")
@EnableDiscoveryClient
@MapperScan("com.donnie.zenitsu.member.dao")
@SpringBootApplication
public class MemberApplication {

	public static void main(String[] args) {
		SpringApplication.run(MemberApplication.class, args);
	}

}
