package com.donnie.zenitsu.member.dao;

import com.donnie.zenitsu.member.entity.MemberStatisticsInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员统计信息
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-17 21:33:36
 */
@Mapper
public interface MemberStatisticsInfoDao extends BaseMapper<MemberStatisticsInfoEntity> {
	
}
