package com.donnie.zenitsu.order.dao;

import com.donnie.zenitsu.order.entity.OrderItemEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单项信息
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-05 21:21:47
 */
@Mapper
public interface OrderItemDao extends BaseMapper<OrderItemEntity> {
	
}
