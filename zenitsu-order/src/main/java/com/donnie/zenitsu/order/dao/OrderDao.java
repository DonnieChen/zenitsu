package com.donnie.zenitsu.order.dao;

import com.donnie.zenitsu.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-10-05 21:21:47
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
