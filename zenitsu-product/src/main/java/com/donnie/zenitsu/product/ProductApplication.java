package com.donnie.zenitsu.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
*  1. 整合Mybatis-Plus
*   1.1 导入依赖
*      <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.2.0</version>
        </dependency>
*   1.2 配置
*       1.2.1 配置数据源
*         1.2.1.1 导入数据库的驱动
*         1.2.1.2 在application.yml配置数据源相关信息
*       1.2.2 配置Mybatis-Plus
*		  1.2.2.1 使用@MapperScan注解
* 		  1.2.2.2 指明Mybatis-plus的sql映射文件位置
*
* */
@MapperScan("com.donnie.zenitsu.product.dao")
@SpringBootApplication
public class ProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductApplication.class, args);
	}

}
