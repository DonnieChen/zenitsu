package com.donnie.zenitsu.product.dao;

import com.donnie.zenitsu.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 01:23:24
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
