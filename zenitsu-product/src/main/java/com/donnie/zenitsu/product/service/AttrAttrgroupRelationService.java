package com.donnie.zenitsu.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.donnie.common.utils.PageUtils;
import com.donnie.zenitsu.product.entity.AttrAttrgroupRelationEntity;

import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 01:23:24
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

