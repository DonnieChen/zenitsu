package com.donnie.zenitsu.product.dao;

import com.donnie.zenitsu.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 01:23:24
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
