package com.donnie.zenitsu.product.dao;

import com.donnie.zenitsu.product.entity.AttrAttrgroupRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性&属性分组关联
 * 
 * @author Donnie
 * @email 461560867@qq.com
 * @date 2020-09-28 01:23:24
 */
@Mapper
public interface AttrAttrgroupRelationDao extends BaseMapper<AttrAttrgroupRelationEntity> {
	
}
