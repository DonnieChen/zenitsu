package com.donnie.zenitsu.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.donnie.zenitsu.product.entity.BrandEntity;
import com.donnie.zenitsu.product.service.BrandService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductApplicationTests {

	@Autowired
	BrandService brandService;

	@Test
	public void testInsert() {
		BrandEntity brandEntity = new BrandEntity();
		brandEntity.setDescript("Donnie_Brand2");
		brandService.save(brandEntity);
		System.out.println("====================保存成功！");
	}

	@Test
	public void testQuery() {
		BrandEntity brandEntity = brandService.getById(1);
		System.out.println("====================查找成功！" + brandEntity.toString());

		BrandEntity brandEntity1 = brandService.getOne(new QueryWrapper<BrandEntity>().eq("brand_id", 2L),true);
		System.out.println("====================查找成功！" + brandEntity1.toString());
	}

	@Test
	public void testUpdate() {
		BrandEntity brandEntity = new BrandEntity();
		brandEntity.setBrandId(1L);
		brandEntity.setDescript("Donnie_Brand3");
		brandService.updateById(brandEntity);

		BrandEntity brandEntityRetrieved = brandService.getById(1L);
		System.out.println("====================更新成功！" + brandEntityRetrieved.getDescript());
	}

}
